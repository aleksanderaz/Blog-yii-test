<?php

namespace app\controllers;

use Yii;
use app\models\Blogtable;
use app\models\BlogtableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



/**
 * BlogtableController implements the CRUD actions for Blogtable model.
 */
class BlogtableController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blogtable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogtableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blogtable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [ 'model' => $this->findModel($id),  ]);
    }

    /**
     * Creates a new Blogtable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blogtable();

      if($this->Ifadmin())
      {

          if ($model->load(Yii::$app->request->post()) && $model->save())
          {
              return $this->redirect(['view', 'id' => $model->id_blog]);
          }
          else
          {
              return $this->render('create', ['model' => $model, ]);
          }
      }
    }

    /**
     * Updates an existing Blogtable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if($this->Ifadmin())
       {
           $model = $this->findModel($id);

           if ($model->load(Yii::$app->request->post()) && $model->save())
           {
               return $this->redirect(['view', 'id' => $model->id_blog]);
           }
           else
           {
               return $this->render('update', ['model' => $model,]);
           }
       }
    }

    /**
     * Deletes an existing Blogtable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->Ifadmin())
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Blogtable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blogtable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
//        if (($model = Blogtable::find($id)) !== null) {
        if (($model = Blogtable::find()->with('idcategories')->andWhere(['id_blog'=>$id])->one()) !== null) {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function Ifadmin()
    {
        if (Yii::$app->user->isGuest)
        {
            Yii::$app->user->loginRequired();
            return false;
        }
        else
        {
            return true;
        }

    }
}
