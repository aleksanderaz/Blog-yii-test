<?php

namespace app\controllers;

use Yii;
use app\models\Blogrelation;
use app\models\BlogrelationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogrelationController implements the CRUD actions for Blogrelation model.
 */
class BlogrelationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blogrelation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogrelationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blogrelation model.
     * @param integer $id_category
     * @param integer $id_blog
     * @return mixed
     */
    public function actionView($id_category, $id_blog)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_category, $id_blog),
        ]);
    }

    /**
     * Creates a new Blogrelation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blogrelation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_category' => $model->id_category, 'id_blog' => $model->id_blog]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blogrelation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_category
     * @param integer $id_blog
     * @return mixed
     */
    public function actionUpdate($id_category, $id_blog)
    {
        $model = $this->findModel($id_category, $id_blog);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_category' => $model->id_category, 'id_blog' => $model->id_blog]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blogrelation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_category
     * @param integer $id_blog
     * @return mixed
     */
    public function actionDelete($id_category, $id_blog)
    {
        $this->findModel($id_category, $id_blog)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blogrelation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_category
     * @param integer $id_blog
     * @return Blogrelation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_category, $id_blog)
    {
        if (($model = Blogrelation::findOne(['id_category' => $id_category, 'id_blog' => $id_blog])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
