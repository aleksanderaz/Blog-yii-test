<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Blogtable;

/**
 * BlogtableSearch represents the model behind the search form about `app\models\Blogtable`.
 */
class BlogtableSearch extends Blogtable
{
  public $categories;
 // public $id_category;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_blog'], 'integer'],
            [['btitle', 'datecreation', 'btext', 'bpict'], 'safe'],
            [['categories'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blogtable::find()->joinWith('idcategories');
//        $query = Blogtable::find()->with(['idcategories'])->joinWith(['productTags'], false)->groupBy('id');
//        $query = Blogtable::find()->with(['idcategories'])->joinWith(['blogrelations'], false)->groupBy('id_blog');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(['query' => $query, ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            Blogtable::tableName().'.id_blog' => $this->id_blog,
            'datecreation' => $this->datecreation,
   //         '{{%blogrelation}}.id_category' => $this->categories,
        ]);

        $query->andFilterWhere(['like', 'btitle', $this->btitle])
            ->andFilterWhere(['like', 'btext', $this->btext])
            ->andFilterWhere(['like', 'bpict', $this->bpict]);

        $query->andFilterWhere(['LIKE', Blogcategor::tableName() . '.name', $this->categories])->groupBy('id_blog');

        return $dataProvider;
    }
}
