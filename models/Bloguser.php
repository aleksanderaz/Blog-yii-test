<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bloguser".
 *
 * @property integer $id_user
 * @property string $loginname
 * @property string $password
 */

class Bloguser extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bloguser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
            [
                [['loginname', 'password'], 'required'],
                [['loginname'], 'string', 'max' => 100],
                [['password'], 'string', 'max' => 50],
                [['testid', 'testlogin', 'testpassw' ], 'safe'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'loginname' => 'Loginname',
            'password' => 'Password',
        ];
    }

    /**
     * @inheritdoc
     */
}
