<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blogcategor".
 *
 * @property integer $id_category
 * @property string $name
 *
 * @property Blogrelation[] $blogrelations
 * @property Blogtable[] $idBlogs
 */
class Blogcategor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogcategor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_category' => 'Id Category',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogrelations()
    {
        return $this->hasMany(Blogrelation::className(), ['id_category' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBlogs()
    {
        return $this->hasMany(Blogtable::className(), ['id_blog' => 'id_blog'])->viaTable('blogrelation', ['id_category' => 'id_category']);
    }
}
