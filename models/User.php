<?php

namespace app\models;


class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

/*   private static $users = [
        '1' => [
            'id' => '100',
            'username' => 'my',
            'password' => 'my',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
    ];

*/
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $loginuser = Bloguser::findOne(['id_user'=>$id]);
        if(!empty($loginuser))
        {
            $user = [
                'id' => $loginuser->id_user,
                'username' => $loginuser->loginname,
                'password' => $loginuser->password,

            ];

            return new static($user);
        }
        else
        {
            return null;
        }
//        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $loginuser = Bloguser::findOne(['loginname'=>$username]);

        if(!empty($loginuser))
        {
            $user = [
                'id' => $loginuser->id_user,
                'username' => $loginuser->loginname,
                'password' => $loginuser->password,
            ];

            return new static($user);
        }
        else
        {
            return null;
        }

//        foreach (self::$users as $user) {
//            if (strcasecmp($user['username'], $username) === 0) {
//                return new static($user);
//            }
//        }

//        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {

    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {

    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
