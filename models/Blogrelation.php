<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blogrelation".
 *
 * @property integer $id_category
 * @property integer $id_blog
 *
 * @property Blogtable $idBlog
 * @property Blogcategor $idCategory
 */
class Blogrelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogrelation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'id_blog'], 'required'],
            [['id_category', 'id_blog'], 'integer'],
            [['id_blog'], 'exist', 'skipOnError' => true, 'targetClass' => Blogtable::className(), 'targetAttribute' => ['id_blog' => 'id_blog']],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Blogcategor::className(), 'targetAttribute' => ['id_category' => 'id_category']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_category' => 'Id Category',
            'id_blog' => 'Id Blog',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBlog()
    {
        return $this->hasOne(Blogtable::className(), ['id_blog' => 'id_blog']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Blogcategor::className(), ['id_category' => 'id_category']);
    }
}
