<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Blogtable */

$this->title = 'Update Blogtable: ' . $model->id_blog;
$this->params['breadcrumbs'][] = ['label' => 'Blogtables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_blog, 'url' => ['view', 'id' => $model->id_blog]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blogtable-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
