<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BlogtableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogtables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogtable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Blogtable', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'pager' => [
            'class' => \kop\y2sp\ScrollPager::className(),
            'container' => '.grid-view tbody',
            'item' => 'tr',
            'paginationSelector' => '.grid-view .pagination',
            'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',],

        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'id_blog',
            'btitle',
            'datecreation',
//            'btext',
            'picters:image',
            [
                'attribute' => 'categories',
                'label' => 'Категории',
                'value' => function ($model) {
                            $ar = array();
                            foreach ($model->categories as $itm)
                            {
                                $ar[] = $itm->name;
                            }
                            return implode(", ", $ar);
                           },
            ],
//            'categoriesname',
 //           [
//                'attribute' => 'categoriesname',
//                'label' => 'Категории',
//                'value' => 'categoriesname',
//            ],
//            [
//                'attribute' => 'namecategories'
//                'label' => 'категория',
//                'value' => function($model){
//        return  $model->getNamecategories();
//    }
//            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<?php Pjax::end(); ?></div>
