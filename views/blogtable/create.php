<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Blogtable */

$this->title = 'Create Blogtable';
$this->params['breadcrumbs'][] = ['label' => 'Blogtables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogtable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
