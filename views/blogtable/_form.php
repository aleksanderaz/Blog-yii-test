<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Blogtable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blogtable-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],]); ?>

    <?= $form->field($model, 'btitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btext')->textarea(['rows' => 6]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'picters:image',
        ],
    ]) ?>

    <?= $form->field($model, 'file')->widget(\kartik\file\FileInput::classname(), ['options' => ['accept' => 'image/*'],]) ?>

    <?= $form->field($model, 'categories')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Blogcategor::find()->all(),'id_category', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Категория', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'maximumInputLength' => 5
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
