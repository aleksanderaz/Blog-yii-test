<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bloguser */

$this->title = 'Create Bloguser';
$this->params['breadcrumbs'][] = ['label' => 'Blogusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bloguser-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
