<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BloguserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogusers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bloguser-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bloguser', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<?php Pjax::begin(); ?>    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model->id_user), ['view', 'id' => $model->id_user]);
        },
    ]) ?>

    <p>
    <?php
    $model = new app\models\Bloguser();
         echo $model->testid." ".$model->testlogin." ".$model->testpassw; ?>

    </p>

    <?php Pjax::end(); ?></div>
