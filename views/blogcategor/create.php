<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Blogcategor */

$this->title = 'Create Blogcategor';
$this->params['breadcrumbs'][] = ['label' => 'Blogcategors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogcategor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
