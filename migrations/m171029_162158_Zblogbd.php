<?php

use yii\db\Migration;

class m171029_162158_Zblogbd extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    /**
     *
     */
    public function up()
    {
        $itm = 25;

        $this->createTable('blogtable', [
            'id_blog' => $this->primaryKey(),
            'btitle' => $this->string(255)->defaultValue(null),
            'datecreation' => $this->date()->defaultValue(null),
            'btext' => $this->text()->defaultValue(null),
            'bpict' => $this->string(255)->defaultValue(null),
        ]);


        for ($i = 1; $i <= $itm; $i++) {
            $this->insert('blogtable', [
                'btitle' => 'Test title '.$i,
                'datecreation' => date("Y.m.d"),
                'btext' => 'Test text blog '.$i,
                'bpict' => '1_1509305086.jpg',
            ]);
        }

        $this->createTable('blogcategor', [
            'id_category' => $this->primaryKey(),
            'name' => $this->string(255)->defaultValue(null),
        ]);
        for ($i = 1; $i <= 4; $i++) {
            $this->insert('blogcategor', [
                'name' => 'Category Test Name '.$i,
            ]);
        }

        $this->createTable('blogrelation', array(
            'id_category' => $this->integer()->notNull(),
            'id_blog' => $this->integer()->notNull(),
            'PRIMARY KEY(id_blog, id_category)',
        ));

        $this->createIndex(
            'id_blog',
            'blogrelation',
            'id_blog'
        );

        $this->addForeignKey(
            'blogrelation_ibfk_1',
            'blogrelation',
            'id_blog',
            'blogtable',
            'id_blog',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
           'id_category',
           'blogrelation',
           'id_category'
       );

        $this->addForeignKey(
            'blogrelation_ibfk_2',
            'blogrelation',
            'id_category',
            'blogcategor',
            'id_category',
            'CASCADE',
            'CASCADE'
        );

        $this->insert('blogrelation', [
            'id_blog' => 1,
            'id_category' => 1,
        ]);
        $this->insert('blogrelation', [
            'id_blog' => 1,
            'id_category' => 2,
        ]);

        for ($i = 2; $i <= $itm; $i++) {
            $this->insert('blogrelation', [
                'id_blog' => $i,
                'id_category' => rand(1, 4),
            ]);
        }

        $this->createTable('bloguser', [
            'id_user' => $this->primaryKey(),
            'loginname' => $this->string(100)->notNull(),
            'password' => $this->string(50)->notNull(),
        ]);

        $this->insert('bloguser', [
            'loginname' => 'root',
            'password' => 'root',
        ]);

    }


    /**
     * @return bool
     */
    public function down()
    {
       $this->dropForeignKey('blogrelation_ibfk_1', 'blogrelation');
       $this->dropForeignKey('blogrelation_ibfk_2', 'blogrelation');
        $this->dropTable('blogrelation');
        $this->dropTable('blogcategor');
        $this->dropTable('blogtable');
        $this->dropTable('bloguser');

//        echo "m171029_050839_Zblogtable cannot be reverted.\n";
//        return false;
        return true;
    }


    /*    public function safeUp()
        {

        }

        public function safeDown()
        {
            echo "m171029_050839_Zblogtable cannot be reverted.\n";

            return false;
        }
    */

}
